package ru.t1.chernysheva.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Getter
    @Setter
    @NotNull
    private Date created = new Date();

    @Getter
    @Setter
    @NotNull
    private List<User> users = new ArrayList<>();

    @Getter
    @Setter
    @NotNull
    private List<Project> projects = new ArrayList<>();

    @Getter
    @Setter
    @NotNull
    private List<Task> tasks = new ArrayList<>();

}
