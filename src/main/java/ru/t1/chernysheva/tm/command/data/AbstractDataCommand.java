package ru.t1.chernysheva.tm.command.data;

import ru.t1.chernysheva.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.model.User;

import java.util.List;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    public AbstractDataCommand() {
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        @Nullable List<Project> projects = serviceLocator.getProjectService().findAll();
        @Nullable List<Task> tasks = serviceLocator.getTaskService().findAll();
        @Nullable List<User> users = serviceLocator.getUserService().findAll();
        if(projects != null) domain.setProjects(projects);
        if(tasks != null) domain.setTasks(tasks);
        if(users != null) domain.setUsers(users);
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
