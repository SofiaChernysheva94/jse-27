package ru.t1.chernysheva.tm.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.model.IWBS;
import ru.t1.chernysheva.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NonNull
    private String name = "";

    @NonNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private String description = "";

    @Nullable
    private Date created = new Date();

    @Nullable
    private String projectId;

}
